package fr.umontpellier.iut.graphes;

import java.util.Objects;

public class Arrete {
    private Integer sommet1;
    private Integer sommet2;

    public Arrete(Integer sommet1, Integer sommet2){
        this.sommet1 = sommet1;
        this.sommet2 = sommet2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Arrete arrete = (Arrete) o;
        return ((Arrete) o).sommet1.equals(sommet1) && ((Arrete) o).sommet2.equals(sommet2) || ((Arrete) o).sommet2.equals(sommet1) && ((Arrete) o).sommet1.equals(sommet2);
    }

    @Override
    public int hashCode() {
        return sommet1 + sommet2;
    }

    public String toString(){
        return "[" + sommet1 + " ; " + sommet2 + "]";
    }
}
