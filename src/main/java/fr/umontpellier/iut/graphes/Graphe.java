package fr.umontpellier.iut.graphes;


import java.lang.reflect.Array;
import java.util.*;

public class Graphe {
    /**
     * matrice d'adjacence du graphe, un entier supérieur à 0 représentant la distance entre deux sommets
     * mat[i][i] = 0 pour tout i parce que le graphe n'a pas de boucle
     */
    private final int[][] mat;

    /**
     * Construit un graphe à n sommets
     *
     * @param n le nombre de sommets du graphe
     */
    public Graphe(int n) {
        mat = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                mat[i][j] = 0;
            }
        }
    }

    /**
     * Construit un graphe par recopie
     */
    public Graphe(Graphe graphe){
        mat = new int[graphe.mat.length][graphe.mat.length];
        for(int i = 0; i < graphe.mat.length; i++){
            for(int j = 0; j < graphe.mat[i].length; j++){
                mat[i][j] = graphe.mat[i][j];
            }
        }
    }

    /**
     * @return le nombre de sommets
     */
    public int nbSommets() {
        return mat.length;
    }

    /**
     * Supprime l'arête entre les sommets i et j
     *
     * @param i un entier représentant un sommet
     * @param j un autre entier représentant un sommet
     */
    public void supprimerArete(int i, int j) {
        mat[i][j] = 0;
        mat[j][i] = 0;
    }

    /**
     * @param i un entier représentant un sommet
     * @param j un autre entier représentant un sommet
     * @param k la distance entre i et j (k>0)
     */
    public void ajouterArete(int i, int j, int k) {
        mat[i][j] = k;
        mat[j][i] = k;
    }

    /*** 
     * @return le nombre d'arête du graphe
     */
    public int nbAretes() {
        int nbAretes = 0;
        for (int i = 0; i < mat.length; i++) {
            for (int j = i + 1; j < mat.length; j++) {
                if (mat[i][j] > 0) {
                    nbAretes++;
                }
            }
        }
        return nbAretes;
    }

    /**
     * @param i un entier représentant un sommet
     * @param j un autre entier représentant un sommet
     * @return vrai s'il existe une arête entre i et j, faux sinon
     */
    public boolean existeArete(int i, int j) {
        return this.mat[i][j] != 0;
    }

    /**
     * @param v un entier représentant un sommet du graphe
     * @return la liste des sommets voisins de v
     */
    public ArrayList<Integer> voisins(int v) {
        ArrayList<Integer> listeVoisins = new ArrayList<>();
        for(int i = 0; i < this.mat.length; i++){
            if(this.existeArete(v,i)) listeVoisins.add(i);
        }
        return listeVoisins;
    }

    /**
     * @return une chaîne de caractères permettant d'afficher la matrice mat
     */
    public String toString() {
        StringBuilder res = new StringBuilder("\n");
        for (int[] ligne : mat) {
            for (int j = 0; j < mat.length; j++) {
                String x = String.valueOf(ligne[j]);
                res.append(x);
            }
            res.append("\n");
        }
        return res.toString();
    }

    /**
     * Calcule la classe de connexité du sommet v
     *
     * @param v un entier représentant un sommet
     * @return une liste d'entiers représentant les sommets de la classe de connexité de v
     */
    public ArrayList<Integer> calculerClasseDeConnexite(int v) {
        ArrayList<Integer> dejaVus = new ArrayList<>();
        ArrayList<Integer> aVoir = new ArrayList<>();
        aVoir.add(v);
        dejaVus.add(v);
        while (!aVoir.isEmpty()){
            int y = aVoir.get(0);
            this.voisins(y).forEach(integer -> {
                if (!dejaVus.contains(integer)) {
                    dejaVus.add(integer);
                    if (!aVoir.contains(integer)) aVoir.add(integer);
                }
            });
            aVoir.remove(0);
        }
        return dejaVus;
    }

    /**
     * @return la liste des classes de connexité du graphe
     */
    public ArrayList<ArrayList<Integer>> calculerClassesDeConnexite() {
        HashSet<ClasseConnexite> tampon = new HashSet<>();
        for (int i = 0; i < this.mat.length-1; i++) {
            tampon.add(new ClasseConnexite(this.calculerClasseDeConnexite(i)));
        }
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        ArrayList<ClasseConnexite> test = new ArrayList<>(tampon);

        for (ClasseConnexite cl : test){
            res.add(cl.getClasse());
        }
        return res;
    }


    /**
     * @return le nombre de classes de connexité
     */
    public int nbCC() {
        return calculerClassesDeConnexite().size();
    }

    /**
     * @param u un entier représentant un sommet
     * @param v un entie représentant un sommet
     * @return vrai si (u,v) est un isthme, faux sinon
     */
     public boolean estUnIsthme(int u, int v) {
        int nbClasseInitial = this.nbCC();
        this.supprimerArete(u,v);
        return this.nbCC() != nbClasseInitial;
     }

    /**
     * Calcule le plus long chemin présent dans le graphe
     *
     * @return une liste de sommets formant le plus long chemin dans le graphe
     */
    public ArrayList<Integer> plusLongChemin() {
        ArrayList<HashMap<Integer, ArrayList<Integer>>> tousLesChemins = new ArrayList<>();
        HashMap<Integer, ArrayList<Integer>> cheminsLongs = new HashMap<>();


        for (int i = 0; i < mat.length; i++) {
            tousLesChemins.add(calculTousLesChemins(i));
        }

        System.out.println(tousLesChemins);

        for (HashMap<Integer, ArrayList<Integer>> chemin : tousLesChemins){
            int distanceLaPlusLongue = -1;
            for (Map.Entry<Integer, ArrayList<Integer>> entry : chemin.entrySet()){
                if (entry.getKey() > distanceLaPlusLongue){
                    distanceLaPlusLongue = entry.getKey();
                }
            }
            cheminsLongs.put(distanceLaPlusLongue, chemin.get(distanceLaPlusLongue));
        }

        int distanceLaPlusLongue = -1;
        for (Map.Entry<Integer, ArrayList<Integer>> entry : cheminsLongs.entrySet()){
            if (entry.getKey() > distanceLaPlusLongue){
                distanceLaPlusLongue = entry.getKey();
            }
        }
        return cheminsLongs.get(distanceLaPlusLongue);

    }


    public HashMap<Integer, ArrayList<Integer>> calculTousLesChemins(int sommet){
        HashMap<Integer, ArrayList<Integer>> resulat = new HashMap<>();
        ArrayList<ArrayList<Integer>> cheminsDejasVus = new ArrayList<>();
        boolean tousLesCheminsParcourus = false;
        int nbChemins = 0;
        while (!tousLesCheminsParcourus){
            nbChemins = resulat.size();
            for (Map.Entry<Integer, ArrayList<Integer>> entry : calculChemin(sommet, cheminsDejasVus).entrySet()){
                resulat.put(entry.getKey(), entry.getValue());
                cheminsDejasVus.add(entry.getValue());
            }
            if (nbChemins == resulat.size()){
                tousLesCheminsParcourus = true;
            }
        }
        return resulat;
    }


    public HashMap<Integer, ArrayList<Integer>> calculChemin(int sommet, ArrayList<ArrayList<Integer>> cheminsDejasVus){
        HashMap<Integer, ArrayList<Integer>> resulat = new HashMap<>();
        ArrayList<Integer> chemin = new ArrayList<>();
        HashSet<Arrete> listeArreteDejaVue = new HashSet<>();

        chemin.add(sommet);

        int distance = 0;
        int sommetEnCours = sommet;
        int voisinChoisi;
        int distanceDuSommet;

        boolean plusDeVoisins = false;
        boolean plusArretes = false;

        int compteur = 1;

        while (!plusDeVoisins && !plusArretes) {

            distanceDuSommet = 0;
            voisinChoisi = -1;

            if (voisins(sommetEnCours).size() == 0) {
                plusDeVoisins = true;
            }

            ArrayList<Integer> voisinsPossibles = new ArrayList<>();

            if (!cheminsDejasVus.isEmpty()){
                ArrayList<ArrayList<Integer>> cheminsParcourusElligibles = cheminsDejasVus;
                for (int i = 0; i < cheminsDejasVus.size(); i++) {
                    if (compteur == cheminsDejasVus.get(i).size()){
                        cheminsParcourusElligibles.remove(i);
                    }
                }
                for (Integer u : voisins(sommetEnCours)) {
                    for (ArrayList<Integer> listeChemins : cheminsParcourusElligibles) {
                        if (!u.equals(listeChemins.get(compteur))) {
                            voisinsPossibles.add(u);
                        }
                    }
                }
            }else{
                voisinsPossibles = voisins(sommetEnCours);
            }

            for (Integer u : voisinsPossibles) {
                if (getValeurArrete(sommetEnCours, u) > distanceDuSommet) {
                    Arrete tampon = new Arrete(sommetEnCours, u);
                    if (!listeArreteDejaVue.contains(tampon)) {
                        distanceDuSommet = getValeurArrete(sommetEnCours, u);
                        voisinChoisi = u;
                    }
                }
            }

            if (voisinChoisi != -1){
                Arrete arrete = new Arrete(sommetEnCours, voisinChoisi);
                listeArreteDejaVue.add(arrete);
                distance += getValeurArrete(sommetEnCours,voisinChoisi);
                sommetEnCours = voisinChoisi;
                chemin.add(voisinChoisi);
                compteur++;
            }else{
                plusArretes = true;
            }

        }

        resulat.put(distance,chemin);
        return resulat;
    }


    public int getValeurArrete(int i, int j){
        return this.mat[i][j];
    }

    /**
     * @return vrai s'il existe un parcours eulérien dans le graphe, faux sinon
     */
    /**
     * @return vrai s'il existe un parcours eulérien dans le graphe, faux sinon
     */
    public boolean existeParcoursEulerien() {
        if(this.estConnexe()) {
            ArrayList<Integer> degresSommets = new ArrayList<>();
            for (int i = 0; i < this.mat.length; i++) {
                degresSommets.add(this.degresDuSommet(i));
            }
            int nbSommetsDegImpair = 0;
            for (Integer sommet : degresSommets) {
                if (sommet % 2 != 0) nbSommetsDegImpair++;
            }
            return nbSommetsDegImpair <= 2;
        }
        return false;
    }

    /**
     * @return vrai si le graphe est un arbre, faux sinon
     */
    public boolean estUnArbre() {
        return this.nbAretes() == this.nbSommets() - 1 && this.estConnexe();
    }

    /**
     *
     * @return vrai si le graphe est connexe, faux sinon
     */
    public boolean estConnexe(){
        ArrayList<Integer> sommets = new ArrayList<>();
        for(int i = 0; i < this.nbSommets(); i++){
            sommets.add(i);
        }
        return this.calculerClasseDeConnexite(this.mat.length-1).containsAll(sommets);
    }

    /**
     *
     * @param i
     * @return le nombre de voisin du sommet i
     */
    public int degresDuSommet(int i){
        return this.voisins(i).size();
    }





}