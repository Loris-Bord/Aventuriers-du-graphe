package fr.umontpellier.iut.graphes;


import org.junit.jupiter.api.Test;
import org.mockito.exceptions.verification.junit.ArgumentsAreDifferent;

import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

public class GrapheTest {

    //ajouter controle des exceptions et boucles infinies

    private static Graphe buildGraphe(int n, int[][] aretes){
        Graphe res = new Graphe(n);
        for(int i=0;i<aretes.length;i++){
            res.ajouterArete(aretes[i][0],aretes[i][1],aretes[i][2]);
        }
        return res;
    }

    @Test
    void testCCdeV() {
        Graphe g = buildGraphe(4,new int[][]{{0,1,1},{2,3,1}});
        System.out.println(g.toString());
        ArrayList<Integer> res = g.calculerClasseDeConnexite(2);
        //conversion en set pour que le equals ci-dessous soit bien une égalité de set et pas une égalité d'arrayList (qui tiendrait compte de l'ordre)
        HashSet<Integer> resSet = new HashSet<>(res);

        HashSet<Integer> answer = new HashSet<>();
        answer.add(3);
        answer.add(2);



        assertEquals(resSet, answer);
    }

    @Test
    void test_voisins(){
        Graphe g = buildGraphe(4,new int[][]{{0,1,1},{2,3,1},{1,2,2},{2,0,3}});
        ArrayList<Integer> toutLesVoisins = new ArrayList<>();
        toutLesVoisins.add(0);
        toutLesVoisins.add(1);
        toutLesVoisins.add(3);
        assertEquals(g.voisins(2), toutLesVoisins);
    }

    @Test
    void testCCdeV2(){
        Graphe g = buildGraphe(4, new int[][]{{0,1,1},{2,3,1}});
        ArrayList<Integer> res = g.calculerClasseDeConnexite(0);
        //conversion en set pour que le equals ci-dessous soit bien une égalité de set et pas une égalité d'arrayList (qui tiendrait compte de l'ordre)
        HashSet<Integer> resSet = new HashSet<>(res);

        HashSet<Integer> answer = new HashSet<>();
        answer.add(0);
        answer.add(1);

        assertEquals(resSet, answer);

    }

    @Test
    void testCC() {
        Graphe g = buildGraphe(4,new int[][]{{0,1,1},{2,3,1}});
        ArrayList<ArrayList<Integer>> res = g.calculerClassesDeConnexite();
        //conversion en set pour que le equals ci-dessous soit bien une égalité de set et pas une égalité d'arrayList (qui tiendrait compte de l'ordre)
        HashSet<HashSet<Integer>> resSet = new HashSet<>();
        for(ArrayList<Integer> cc : res){
            resSet.add(new HashSet<>(cc));
        }

        HashSet<Integer> cc1 = new HashSet<>();
        cc1.add(3);
        cc1.add(2);

        HashSet<Integer> cc2 = new HashSet<>();
        cc2.add(0);
        cc2.add(1);

        HashSet<HashSet<Integer>> answer = new HashSet<>();
        answer.add(cc1);
        answer.add(cc2);
        //System.out.println(resSet);

        assertEquals(resSet, answer);
    }

    @Test
    void testCCPlusDur() {
        Graphe g = buildGraphe(4,new int[][]{{2,3,1},{2,1,1},{1,3,1}});
        System.out.println(g.toString());
        ArrayList<ArrayList<Integer>> res = g.calculerClassesDeConnexite();
        System.out.println(res);
    }

    @Test
    void testCCArbre(){
        Graphe g = buildGraphe( 4, new int[][]{{3,0,1},{2,3,1},{2,1,1},{1,3,1},{1,0,1}});
        System.out.println(g.calculerClassesDeConnexite());
    }

    @Test
    void testEstUnIsthme() {
        Graphe g = buildGraphe(4,new int[][]{{0,1,1},{2,3,1},{2,1,1},{1,3,1}});
        assertTrue(g.estUnIsthme(0,1));
        assertFalse(g.estUnIsthme(1,3));
    }

    @Test
    void testPlusLongChemin() {
        Graphe g = buildGraphe(5,new int[][]{{3,4,1},{4,1,1},{0,1,1},{2,3,1},{2,1,1},{1,3,1}});

        ArrayList<Integer> L = new ArrayList<Integer>();
        L.add(0);
        L.add(1);
        L.add(2);
        L.add(3);
        L.add(1);
        L.add(4);
        L.add(3);
        assertEquals(g.plusLongChemin(),L);
    }

    @Test
    void testCheminLepLusLongGraphePlusComplexe(){
        Graphe g = buildGraphe(12, new int[][]{{0,1,3},{0,5,9},{0,8,2},{1,2,2},{2,3,3},{3,4,5},{1,5,4},{5,3,1},{6,4,5},{4,7,5},{5,6,2},{6,7,9},{8,5,6},{8,9,9},{9,6,1},{9,10,2},{10,11,2},{6,11,6},{11,7,3}});
        System.out.println(g.plusLongChemin());
    }

    @Test
    void testCheminLePlusLong(){
        Graphe g = buildGraphe(5,new int[][]{{3,4,1},{4,1,1},{0,1,1},{2,3,1},{2,1,1},{1,3,1}});
        ArrayList<Integer> L = new ArrayList<Integer>();
        L.add(0);
        L.add(1);
        L.add(2);
        L.add(3);
        L.add(1);
        L.add(4);
        L.add(3);
        System.out.println(L);
        System.out.println(g.plusLongChemin());
    }


    @Test
    void testExisteParcoursEulerien() {
        Graphe g = buildGraphe(5,new int[][]{{0,1,1},{1,2,1},{2,0,1},{1,3,1},{2,4,1}});

        assertFalse(g.existeParcoursEulerien());
    }

    @Test
    void testEstUnArbre() {
        Graphe g = buildGraphe( 13, new int[][]{{0,1,1},{1,2,1},{2,3,1},{2,4,1},{2,8,1},{4,5,1},{5,6,1},{5,7,1},{8,9,1},{8,10,1},{8,11,1},{11,12,1}});
        assertTrue(g.estUnArbre());
    }

    @Test
    void testExisteParcoursEulerienTrue(){
        Graphe g = buildGraphe(4, new int[][]{{0,1,1},{1,2,1},{2,3,1},{3,1,1}});
        assertTrue(g.existeParcoursEulerien());
    }

    @Test
    void testExisteParcoursEulerienFalse(){
        Graphe g = buildGraphe(6, new int[][]{{0,1,1},{1,2,1},{2,3,1},{3,4,1},{4,1,1},{4,5,1}});
        assertFalse(g.existeParcoursEulerien());
    }

    @Test
    void testExisteParcoursEulerienTrue10Sommets(){
        Graphe g = buildGraphe(10, new int[][]{{0,1,1},{1,2,1},{2,3,1},{3,4,1},{4,5,1},{5,6,1},{6,7,1},{7,8,1},{8,9,1},{9,0,1}});
        assertTrue(g.existeParcoursEulerien());
    }

    @Test
    void testExisteParcoursEulerienOuvertTrue(){
        Graphe g = buildGraphe(5, new int[][]{{0,1,1},{1,2,1},{2,3,1},{3,4,1}});
        assertTrue(g.existeParcoursEulerien());
    }

    @Test
    void testEstUnArbreFalse(){
        Graphe g = buildGraphe(4, new int[][]{{0,1,1},{1,2,1},{2,3,1},{3,1,1}});
        assertFalse(g.estUnArbre());
    }




}
